#include <X11/XF86keysym.h>
static const unsigned int borderpx  = 2;        /* border pixel of windows */
static const unsigned int gappx     = 7;        /* gaps between windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const unsigned int systraypinning = 1;   /* 0: sloppy systray follows selected monitor, >0: pin systray to monitor X */
static const unsigned int systrayspacing = 2;   /* systray spacing */
static const int          systraypinningfailfirst = 1;   /* 1: if pinning fails, display systray on the first monitor, False: display systray on the last monitor*/
static const int          showsystray        = 1;     /* 0 means no systray */
static const int          showbar            = 0;        /* 0 means no bar */
static const int          topbar             = 1;        /* 0 means bottom bar */
static const char        *fonts[]       = { "monospace:size=10" };
static const char         dmenufont[]       = "monospace:size=10";
static const char         col_gray1[]       = "#222222";
static const char         col_gray2[]       = "#444444";
static const char         col_gray3[]       = "#bbbbbb";
static const char         col_gray4[]       = "#eeeeee";
static const char         col_cyan[]        = "#20821d";
static const char        *colors[][3]      = {
/*               fg         bg         border   */
[SchemeNorm] = { col_gray3, col_gray1, col_gray2 },
[SchemeSel]  = { col_gray4, col_cyan,  col_cyan  },
};

static const char *const autostart[] = {
"picom", "--config", "/home/jake/.xmonad/scripts/picom.conf", NULL      ,
"/home/jake/.scripts/wallpaper.sh"                          , "draw"    , NULL,
"pasystray"                                                 , "&"       , NULL,
"emacs"                                                     , "--daemon", NULL,
"sxhkd"                                                     , NULL      ,
"slstatus"                                                  , NULL      ,
"nm-applet"                                                 , "&"       , NULL,
"volumeicon"                                                , "&"       , NULL,
"blueberry-tray"                                            , "&"       , NULL,
"numlockx"                                                  , "on"      , "&" , NULL,
"/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1" , "&"       , NULL,
"/usr/lib/xfce4/notfyd/xfce4-notifyd"                       , "&"       , NULL,
NULL /* terminate */
};

static const char *tags[] = { "Main", "Term", "Emacs", "Mail", "Pdf", "Games", "Video", "8", "Web" };

static const Rule rules[] = {
/* xprop(1):
 *	WM_CLASS(STRING) = instance, class
 *	WM_NAME(STRING) = title
 */
/* class      instance    title       tags mask     isfloating   monitor */
    { "Gimp"          , NULL  ,     NULL         ,       0     ,       1,           -1 },
    { "pavucontrol-qt", NULL  ,     NULL         ,       0     ,       1,           -1 },
    { "heroic"        , NULL  ,     NULL         ,       1 << 5,       0,           -1 },
    { "Steam"         , NULL  ,     NULL         ,       1 << 5,       0,           -1 },
    { "Steam"         ,"Steam",    "Friends List",       0     ,       1,           -1 },
    { "Steam"         ,"Steam",    "Steam - News",       0     ,       1,           -1 },
    { "Lutris"        , NULL  ,     NULL         ,       1 << 5,       0,           -1 },
    { "mpv"           , NULL  ,     NULL         ,       1 << 6,       0,           -1 },
    { "Zathura"       , NULL  ,     NULL         ,       1 << 4,       0,           -1 },
    { "Brave-browser" , NULL  ,     NULL         ,       1 << 8,       0,           -1 },
    { "Alacritty"     , NULL  ,     NULL         ,       1 << 1,       0,           -1 },
    { "Emacs"         , NULL  ,     NULL         ,       1 << 2,       0,           -1 },
    { "Geary"         , NULL  ,     NULL         ,       1 << 3,       0,           -1 },

};

static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */ static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */ static const Layout layouts[] = {/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
};

#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} }, \
	{ MODKEY|Mod1Mask,              KEY,      tagnextmon,     {.ui = 1 << TAG} }, \
	{ MODKEY|Mod1Mask|ShiftMask,    KEY,      tagprevmon,     {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

static char        dmenumon[2]  = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[]   = { "dmenu_run"                         , "-c"   , "-l", "15", NULL };
static const char *leftcmd[]    = { "/home/jake/.scripts/monitor.sh"    , "left" , NULL };
static const char *bothcmd[]    = { "/home/jake/.scripts/monitor.sh"    , "both" , NULL };
static const char *rightcmd[]   = { "/home/jake/.scripts/monitor.sh"    , "right", NULL };
static const char *gamescmd[]   = { "/home/jake/.scripts/monitor.sh"    , "games", NULL };
static const char *slockcmd[]   = { "slock"                             ,          NULL };
static const char *volupcmd[]   = { "/home/jake/.scripts/volume-control", "up"   , NULL };
static const char *voldowncmd[] = { "/home/jake/.scripts/volume-control", "down" , NULL };
static const char *volmutecmd[] = { "/home/jake/.scripts/volume-control", "mute" , NULL };

static Key keys[] = {
/* modifier                     key        function        argument */ // use 'xev | grep keysym' to find key names
{ MODKEY,                       XK_semicolon,           spawn,          {.v = slockcmd } },
{ MODKEY|Mod1Mask,              XK_Return,              spawn,          {.v = dmenucmd } },
{ MODKEY,                       XK_b,                   togglebar,      {0} },
{ MODKEY,                       XK_l,                   focusstack,     {.i = +1 } },
{ MODKEY,                       XK_h,                   focusstack,     {.i = -1 } },
{ MODKEY,                       XK_comma,               incnmaster,     {.i = +1 } },
{ MODKEY,                       XK_period,              incnmaster,     {.i = -1 } },
{ MODKEY,                       XK_j,                   setmfact,       {.f = -0.05} },
{ MODKEY,                       XK_k,                   setmfact,       {.f = +0.05} },
{ MODKEY,                       XK_Tab,                 zoom,           {0} },
{ MODKEY|ShiftMask,             XK_Tab,                 view,           {0} },
{ MODKEY|ShiftMask,             XK_c,                   killclient,     {0} },
{ MODKEY,                       XK_t,                   setlayout,      {.v = &layouts[0]} },
{ MODKEY|ShiftMask,             XK_f,                   setlayout,      {.v = &layouts[1]} },
{ MODKEY|ShiftMask,             XK_space,               setlayout,      {.v = &layouts[2]} },
{ MODKEY,                       XK_space,               setlayout,      {0} },
{ MODKEY,                       XK_f,                   togglefloating, {0} },
{ MODKEY,                       XK_m,                   togglefullscr,  {0} },
{ MODKEY,                       XK_0,                   view,           {.ui = ~0 } },
{ MODKEY|ShiftMask,             XK_0,                   tag,            {.ui = ~0 } },
{ MODKEY,                       XK_minus,               setgaps,        {.i = -1 } },
{ MODKEY,                       XK_equal,               setgaps,        {.i = +1 } },
{ MODKEY|ShiftMask,             XK_equal,               setgaps,        {.i = 0  } },
{ MODKEY,                       XK_e,                   focusmon,       {.i = -1 } },
{ MODKEY,                       XK_r,                   focusmon,       {.i = +1 } },
{ MODKEY|ShiftMask,             XK_e,                   tagmon,         {.i = -1 } },
{ MODKEY|ShiftMask,             XK_r,                   tagmon,         {.i = +1 } },
TAGKEYS(                        XK_1,                      0)
TAGKEYS(                        XK_2,                      1)
TAGKEYS(                        XK_3,                      2)
TAGKEYS(                        XK_4,                      3)
TAGKEYS(                        XK_5,                      4)
TAGKEYS(                        XK_6,                      5)
TAGKEYS(                        XK_7,                      6)
TAGKEYS(                        XK_8,                      7)
TAGKEYS(                        XK_9,                      8)
{ MODKEY|ShiftMask,             XK_q,                    quit,           {0} },
{ MODKEY,                       XK_F9,                   spawn,          {.v = gamescmd } },
{ MODKEY,                       XK_F10,                  spawn,          {.v = leftcmd } },
{ MODKEY,                       XK_F11,                  spawn,          {.v = bothcmd } },
{ MODKEY,                       XK_F12,                  spawn,          {.v = rightcmd } },
{ MODKEY,                       XK_q,                    quit,           {1} },
{ MODKEY|ControlMask,           XK_space,                focusmaster,    {0} },
{ 0,                            XF86XK_AudioMute,        spawn,          {.v = volmutecmd } },
{ 0,                            XF86XK_AudioLowerVolume, spawn,          {.v = voldowncmd } },
{ 0,                            XF86XK_AudioRaiseVolume, spawn,          {.v = volupcmd } },
};

static Button buttons[] = {
/* click                event mask      button          function        argument */
{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
{ ClkTagBar,            0,              Button1,        view,           {0} },
{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};
